# Asymical 
**Asymical** est un thème enfant pour une boutique fictive du thème WooCommerce Officiel - [Storefront](https://woocommerce.com/storefront/).

## Utilisation
Vu que  **Asymical** est un thème enfant de Storefront, vous avez besoin d'installer le thème Storefront ainsi que le plugin [WooCommerce](https://woocommerce.com) avant de l'activer.

## Installation
1. Télécharger et installer [Storefront](https://wordpress.org/themes/storefront/).
2. [Télécharger Asymical](https://gitlab.com/remihayrimbault/asymical-woocommerce.git).
3. Dans le back-office de votre site, allez dans Apparence -> Themes et cliquer sur le bouton Ajouter un nouveau. 
4. Uploader asymical-woocommerce.zip en utilisant le bouton d'ajout de thème.
5. Allez dans Apparence > Themes et activer le thème.

## Lien
* Demo: https://preprod.remihr.fr/