<footer class="footer">
    <div class="footer__row">
        <a href="<?php bloginfo('url'); ?>">
            <img class="footer__logo" src="<?php the_field('logo_inverse','option'); ?>" alt="Asymical">
        </a>
    </div>
    <div class="footer__row">
        <?php wp_nav_menu(array('theme_location' => 'menu-footer','menu_class' => 'footer__list', 'container'=> false));?>
    </div>
    <div class="footer__row">
        <ul class="footer__rs">
            <?php 
                $links = get_field('reseaux_sociaux','option');
                foreach ($links as $link) :
            ?>
                <li class="footer__link">
                    <a href="<?= $link['lien'] ?>">
                        <i class="<?= $link['icone'] ?>"></i>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>    
</footer>
<?php  wp_footer(); ?>
</body>
</html>