<?php

add_action( 'wp_enqueue_scripts', 'asymical_enqueue_styles' );
function asymical_enqueue_styles() {

    wp_enqueue_style('font', 'https://fonts.googleapis.com/css2?family=Poppins:wght@800&display=swap'); //add font Poppins

    wp_enqueue_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css'); //add font-awesome CDN

    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); // add Global Css file

    wp_enqueue_style( 'footer-style', get_template_directory_uri() . '-child/styles/footer.css' ); // add Footer Css file
}

// Add Footer ACF Options Page
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Pied de page');
}

register_nav_menus( array(
    'menu-footer' => 'Menu pied de page',
) );

?>